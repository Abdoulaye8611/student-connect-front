import { Component, OnInit } from "@angular/core";
import { StudentMarksService } from "../shared.services/student-marks.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MarkModel } from "../shared/models/mark.model";
import { WorkAndMarkModel } from "../shared/models/workandmark.model";
import { StudentWorkdisplayService } from "../shared.services/student-workdisplay.service";

@Component({
  selector: "app-notes",
  templateUrl: "./studentmarks.component.html",
  styleUrls: ["./studentmarks.component.css"]
})
export class MarksComponent implements OnInit {
  marks: MarkModel[] = new Array();
  private currentWorksAndMarks: WorkAndMarkModel[];
  private studentid: WorkAndMarkModel[];
  works: WorkAndMarkModel[];

  constructor(
    private studentMarksService: StudentMarksService,
    private studentWorkdisplayservice: StudentWorkdisplayService,
    private route: ActivatedRoute
  ) {
    this.studentid = this.route.snapshot.params.studentid;
  }

  ngOnInit() {
    this.studentWorkdisplayservice
      .getAllStudentWorksForStudent(this.studentid)
      .subscribe(
        value => {
          this.works = value;
          console.log(value);
        },
        error => {
          console.log(error);
        }
      );
  }
}
