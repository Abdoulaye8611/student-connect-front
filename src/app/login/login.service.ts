import {Injectable} from '@angular/core';

import {LoginModel} from '../shared/models/login.model';
import {Observable, Subject} from 'rxjs';
import 'rxjs-compat/add/observable/of';
import {AppSettings} from '../shared.services/AppSettings';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private baseUrl = AppSettings.API_ENDPOINT;
  private headers = new HttpHeaders({'Content-Type': 'application/json'});
  private options = {headers: this.headers};
  private subject = new Subject<any>();

  constructor(private http: HttpClient) {
  }


  login(email, password): void {

    this.http.get<LoginModel>(this.baseUrl + '/user/' + email + '/').subscribe(userAccepted => {
      localStorage.setItem('currentUser', userAccepted.id.toString());
      localStorage.setItem('userType', userAccepted.type.toString());
      this.sendLogin(userAccepted.id.toString(), userAccepted.type.toString());
      return Observable.of(userAccepted);
    });
  }

  sendLogin(loginId: string, type: string) {
    this.subject.next({loginId: loginId, type: type});
  }

  getLoginObservable(): Observable<any> {
    return this.subject.asObservable();

  }

  currentUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  logout() {
    return localStorage.setItem('currentUser', null);
  }

}

