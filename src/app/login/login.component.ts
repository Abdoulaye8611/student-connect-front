import {Component, OnInit} from '@angular/core';

import {LoginModel} from '../shared/models/login.model';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {LoginService} from './login.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: LoginModel = new LoginModel(null, null, null, null);
  loginForm: FormGroup;
  hide = true;
  studentid: LoginModel;


  constructor(private formBuilder: FormBuilder, private loginService: LoginService, private _router: Router) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      'email': [this.user.email, [
        Validators.required,
        Validators.email
      ]],
      'password': [this.user.password, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ]]
    });
  }

  onLoginSubmit() {
    console.log(this.user, this.user.email + ' ' + this.user.password);
    this.loginService.login(this.user.email, this.user.password);
    this._router.navigate(['student/{studentid}/courses']);

  }

}
