import { Component, OnInit } from '@angular/core';
import {StudentModel} from '../shared/models/student.model';
import {StudentdashService} from '../studentdash/studentdash.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-student-update',
  templateUrl: './student-update.component.html',
  styleUrls: ['./student-update.component.css']
})
export class StudentUpdateComponent implements OnInit {
  private student: StudentModel;

  constructor(private studentdashService: StudentdashService, private _router: Router ) { }

  ngOnInit() {
    this.student=this.studentdashService.getter();
  }

  updateForm(){
    if(this.student.id == undefined){
      this.studentdashService.createStudent(this.student).subscribe((student)=>{
        console.log(student);
       this._router.navigate(['/students']);
      },(error)=>{
        console.log(error);
      });
    }else{
      this.studentdashService.updateStudent(this.student).subscribe((student)=>{
        console.log(student);
        this._router.navigate(['/students']);
      },(error)=>{
        console.log(error);
      });
    }
  }

  returnHomePage() {
   return this._router.navigate(['/students']);
  }

}
