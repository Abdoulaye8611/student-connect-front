import { Component, OnInit } from '@angular/core';
import {NavigationEnd, Route, Router} from '@angular/router';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-teacher-nav-bar',
  templateUrl: './teacher-nav-bar.component.html',
  styleUrls: ['./teacher-nav-bar.component.css']
})
export class TeacherNavBarComponent implements OnInit {
  teacherid: string;
  isNavVisible: Boolean = false;

  constructor(private route: Router) { }

  ngOnInit() {
    this.teacherid = localStorage.id;
    this.route.events.pipe(
      filter(e => e instanceof NavigationEnd)
    ).subscribe( value => {
      console.log(location.pathname);
      this.isNavVisible = location.pathname.indexOf('auth') < 0;
    });
  }

}
