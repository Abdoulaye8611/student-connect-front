import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MarkModel} from '../shared/models/mark.model';
import {Observable} from 'rxjs';
import {AppSettings} from './AppSettings';

@Injectable({
  providedIn: 'root'
})
export class StudentMarksService {
  private baseUrl = AppSettings.API_ENDPOINT;
  private headers = new HttpHeaders({'Content-Type': 'application/json'});
  private options = {headers: this.headers};
  private mark = new MarkModel();
  constructor(private http: HttpClient) { }

  getMarks(): Observable <MarkModel[]> {
    return this.http.get<MarkModel[]>(this.baseUrl + '/mark')
      .catch(this.errorHandler);
  }

  getMark(id: Number) {
    return this.http.get(this.baseUrl + '/mark/' + id)
      .catch(this.errorHandler);
  }

  deleteMark(id: Number) {
    return this.http.delete(this.baseUrl + '/mark/' + id, this.options).map((response: Response) => response)
      .catch(this.errorHandler);
  }

  createMark(mark: MarkModel) {
    return this.http.post(this.baseUrl + '/mark', JSON.stringify(this.mark), this.options).map((response: Response) => response)
      .catch(this.errorHandler);
  }

  updateMark(mark: MarkModel) {
    return this.http.put(this.baseUrl + '/mark', JSON.stringify(mark),  this.options)
      .catch(this.errorHandler);
  }

  errorHandler(error: Response) {

    return Observable.throw(error || 'SERVER ERROR');
  }

  setter (mark: MarkModel) {
    this.mark = mark;
  }

  getter() {
    return this.mark;
  }
}
