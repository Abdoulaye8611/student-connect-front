import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StudentModel} from '../shared/models/student.model';
import {WorkAndMarkModel} from '../shared/models/workandmark.model';
import {AppSettings} from './AppSettings';

@Injectable({
  providedIn: 'root'
})
export class StudentWorkdisplayService {
  private baseUrl = AppSettings.API_ENDPOINT;
  private headers = new HttpHeaders({'Content-Type': 'application/json'});
  private options = {headers: this.headers};
  private student = new StudentModel();

  constructor(private http: HttpClient) { }

  getAllStudent(): Observable <StudentModel[]> {
    return this.http.get<StudentModel[]>(this.baseUrl + '/student')
      .catch(this.errorHandler);
  }

  getAllStudentWorksForTeacher(teacherid, studentid): Observable <WorkAndMarkModel[]> {
    return this.http.get<WorkAndMarkModel[]>(this.baseUrl + '/teacher/' + teacherid + '/' + studentid  + '/marks')
      .catch(this.errorHandler);
  }


  getAllStudentWorksForStudent(studentid): Observable <WorkAndMarkModel[]> {
    return this.http.get<WorkAndMarkModel[]>(this.baseUrl + '/student/' + studentid + '/work')
      .catch(this.errorHandler);
  }


  setMark(workid, markValue): Observable <WorkAndMarkModel[]> {
    let input = new FormData();
    input.append('mark', markValue);
    return this.http.post<WorkAndMarkModel[]>(this.baseUrl + '/work/' + workid + '/mark' , input);
  }

  errorHandler(error: Response) {

    return Observable.throw(error || 'SERVER ERROR');
  }

  setter(student: StudentModel) {
    this.student = student;
  }

  getter() {
    return this.student;
  }

}
