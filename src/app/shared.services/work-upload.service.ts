import { Injectable } from '@angular/core';
import {WorkModel} from '../shared/models/work.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorkUploadService {

  private baseUrl = 'http://localhost:8282/api';
  private headers = new HttpHeaders();
  private options = {headers: this.headers};
  constructor( private http: HttpClient ) {
    this.subject.subscribe(x => console.dir(x));
  }

  private subject: Subject<WorkModel[]> = new BehaviorSubject([]);

  createWork(work: WorkModel, studentid: string, courseid: string) {
    var input = new FormData();
    input.append('file', work.file);
    input.append('studentid', studentid);
    input.append('courseid', courseid);

    return this.http.post( this.baseUrl + '/student/work/create' , input)
      .do(
        (x) => {},
        (err) => {},
        () => {this.getWorks(); }
      )
      .catch(this.errorHandler);
  }


  getWorks(): Observable<WorkModel[]> {
    this.http.get<WorkModel[]>( this.baseUrl + '/work')
      .catch(this.errorHandler).subscribe((x) => {this.subject.next(x)},(err) => {this.subject.error(err)});
    return this.subject;
  }


  hasWork(studentid: string, courseid: string){
    return this.http.get(this.baseUrl + '/work/hasWork/' + studentid + '/' + courseid );
  }

  errorHandler(error: Response) {

    return Observable.throw(error || 'SERVER ERROR');
  }




}
