import {Injectable} from '@angular/core';
import {CourseModel} from '../shared/models/course.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject, Subject} from 'rxjs';
import {AppSettings} from './AppSettings';


@Injectable({
  providedIn: 'root'
})

export class CourseCreateService {

  private baseUrl = AppSettings.API_ENDPOINT;
  private headers = new HttpHeaders();
  private options = {headers: this.headers};

  constructor(private http: HttpClient) {
    this.subject.subscribe(x => console.dir(x));
  }

  private subject: Subject<CourseModel[]> = new BehaviorSubject([]);

  createCourse(teacherid: string, course: CourseModel) {
    const input = new FormData();
    input.append('name', course.name);
    input.append('file', course.file);

    return this.http.post(this.baseUrl + '/teacher/' + teacherid + '/course/create', input, this.options)
      .do(
        (x) => {
        },
        (err) => {
        },
        () => {
          this.getCourses(teacherid);
        }
      )
      .catch(this.errorHandler);
  }


  getCourses(teacherid: string): Observable<CourseModel[]> {
    this.http.get<CourseModel[]>(this.baseUrl + '/teacher/' + teacherid + '/courses')
      .catch(this.errorHandler).subscribe((x) => {
      this.subject.next(x);
    }, (err) => {
      this.subject.error(err);
    });
    return this.subject;
  }


  getAllCourses(): Observable<CourseModel[]> {
    this.http.get<CourseModel[]>(this.baseUrl + '/courses')
      .catch(this.errorHandler).subscribe((x) => {
      this.subject.next(x);
    }, (err) => {
      this.subject.error(err);
    });
    return this.subject;
  }

  errorHandler(error: Response) {

    return Observable.throw(error || 'SERVER ERROR');
  }


}
