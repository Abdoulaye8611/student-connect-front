import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {StudentModel} from "../shared/models/student.model";
import {StudentdashService} from "../studentdash/studentdash.service";
import {TeacherModel} from "../shared/models/teacher.model";
import {TeacherdashService} from "../teacherdash/teacherdash.service";

@Component({
  selector: 'app-teacherupdate',
  templateUrl: './teacherupdate.component.html',
  styleUrls: ['./teacherupdate.component.css']
})
export class TeacherupdateComponent implements OnInit {
  private teacher: TeacherModel;

  constructor(private teacherdashService: TeacherdashService, private _router: Router ) { }

  ngOnInit() {
    this.teacher = this.teacherdashService.getter();
  }

  teacherUpdateForm(){
    if(this.teacher.id==undefined){
      this.teacherdashService.createTeacher(this.teacher).subscribe((teacher)=>{
        console.log(teacher);
        this._router.navigate(['/teachers']);
      },(error)=>{
        console.log(error);
      });
    } else{
      this.teacherdashService.updateTeacher(this.teacher).subscribe((teacher)=>{
        console.log(teacher);
        this._router.navigate(['/teachers']);
      },(error)=>{
        console.log(error);
      });
    }
  }

  returnHomePage() {
    return this._router.navigate(['/teachers']);
  }
}
