import { Component, OnInit } from '@angular/core';
import {StudentdashService} from './studentdash.service';
import {StudentModel} from '../shared/models/student.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-studentdash',
  templateUrl: './studentdash.component.html',
  styleUrls: ['./studentdash.component.css']
})
export class StudentdashComponent implements OnInit {
  private students: StudentModel[];


  constructor(private studentdashService: StudentdashService, private _router: Router) { }

  ngOnInit() {
    this.studentdashService.getStudents().subscribe( value => {
        this.students = value;
        console.log(value);
      }, (error) => {
        console.log(error);
      }
    );

  }

  newStudent() {
    const student = new StudentModel();
    this.studentdashService.setter(student);
    this._router.navigate(['/studentupdate']);
  }

  deleteStudent(student) {
    this.studentdashService.deleteStudent(student.id).subscribe((data) => {
      this.students.splice(this.students.indexOf(student), 1);
    }, (error) => {
      console.log(error);
    });
  }

  updateStudent(student) {
    this.studentdashService.setter(student);
    this._router.navigate(['/studentupdate']);
  }

}
