import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {StudentModel} from '../shared/models/student.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class        StudentdashService {
  private baseUrl = 'http://localhost:8282/api';
  private headers = new HttpHeaders({'Content-Type': 'application/json'});
  private options = {headers: this.headers};
  private student = new StudentModel();
  constructor(private http: HttpClient) { }


  getStudents(): Observable <StudentModel[]> {
    return this.http.get<StudentModel[]>(this.baseUrl + '/student')
      .catch(this.errorHandler);
    }

   getStudent(id: Number) {
    return this.http.get(this.baseUrl + '/student/' + id)
    .catch(this.errorHandler);
  }


 deleteStudent(id: Number) {
     return this.http.delete(this.baseUrl + '/student/' + id, this.options).map((response: Response) => response)
    .catch(this.errorHandler);
 }

  createStudent(student: StudentModel) {
    return this.http.post(this.baseUrl + '/student', JSON.stringify(student), this.options).map((response: Response) => response)
    .catch(this.errorHandler);
 }

  updateStudent(student: StudentModel) {
    return this.http.put(this.baseUrl + '/student', JSON.stringify(student),  this.options)
      .catch(this.errorHandler);
  }

  errorHandler(error: Response) {

    return Observable.throw(error || 'SERVER ERROR');
  }

  setter(student: StudentModel) {
    this.student = student;
  }

  getter() {
    return this.student;
  }


}
