import {Component, Input, OnInit} from '@angular/core';
import {CourseModel} from '../shared/models/course.model';
import {CourseCreateService} from '../shared.services/course-create.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-student-courses',
  templateUrl: './student-course.component.html',
  styleUrls: ['./student-course.component.css']
})
export class StudentCourseComponent implements OnInit {
  private courses: CourseModel[];
  private studentid: string;


  constructor(private courseCreateService: CourseCreateService, private router: Router) {
    this.courseCreateService.getAllCourses().subscribe( value => {
      this.courses = value;
      console.log(value);
    }, (error) => {
      console.log(error);
    });

  }

  ngOnInit() {
    this.studentid = '1';
  }



}
