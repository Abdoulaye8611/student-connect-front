import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import {CreateCourseComponent} from './create-course/create-course.component';
import {StudentdashComponent} from './studentdash/studentdash.component';
import {StudentUpdateComponent} from './student-update/student-update.component';
import {TeacherupdateComponent} from './teacherupdate/teacherupdate.component';
import {TeacherdashComponent} from './teacherdash/teacherdash.component';

import {DetailsUploadComponent} from './upload/details-upload.component';
import {StudentCourseComponent} from './student-course/student-course.component';
import {CourseListComponent} from './course-list/course-list.component';
import {WorkUploadComponent} from './work-upload/work-upload.component';
import {MarksComponent} from './studentmarks/studentmarks.component';
import {TeacherWorkdisplayComponent} from './teacher-workdisplay/teacher-workdisplay.component';

const routes: Routes = [{
  path: '',
  component: LoginComponent
}, {
  path: 'auth/register',
  component: RegisterComponent
}, {
  path: 'auth/login',
  component: LoginComponent
},
  {
    path: 'students',
    component: StudentdashComponent
  },
  {
    path: 'studentupdate',
    component: StudentUpdateComponent
  },
  {
    path: 'courselist',
    component: CourseListComponent
  },
  {
    path: 'teacherupdate',
    component: TeacherupdateComponent
  },
  {
    path: 'teachers',
    component: TeacherdashComponent
  },

  {
    path: 'student/:studentid/marks',
    component: MarksComponent
  },
  {
    path: 'coursedetail',
    component: DetailsUploadComponent
  },
  {
    path: 'teacher/:teacherid/marks',
    component: TeacherWorkdisplayComponent
  },
  {
    path: 'student/:studentid/courses',
    component: StudentCourseComponent
  },

  {
    path: 'student/:studentid/works',
    component: WorkUploadComponent
  },

  {
    path: 'teacher/:teacherid/courses',
    component: CreateCourseComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
