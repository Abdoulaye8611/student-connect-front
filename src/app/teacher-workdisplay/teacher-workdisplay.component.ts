import {Component, OnInit} from '@angular/core';
import {StudentWorkdisplayService} from '../shared.services/student-workdisplay.service';
import {ActivatedRoute, Router} from '@angular/router';
import {StudentModel} from '../shared/models/student.model';
import {WorkAndMarkModel} from '../shared/models/workandmark.model';

@Component({
  selector: 'app-teacher-displaymarks',
  templateUrl: './teacher-workdisplay.component.html',
  styleUrls: ['./teacher-workdisplay.component.css']
})
export class TeacherWorkdisplayComponent implements OnInit {

  private students: StudentModel[];
  private teacherid: WorkAndMarkModel;
  private studentid: WorkAndMarkModel;
   selectedStudent: StudentModel;
   private currentWorksAndMarks: WorkAndMarkModel[];

  constructor(private studentWorkdisplayService: StudentWorkdisplayService, private route: ActivatedRoute ) {
    this.teacherid = this.route.snapshot.params.teacherid;
    this.studentid = this.route.snapshot.params.studentid;
  }

  ngOnInit() {
    this.studentWorkdisplayService.getAllStudent().subscribe(  value => {
      this.students = value;
      console.log(value);
    }, (error) => {
      console.log(error);
    });
  }

 getAllStudentWorksForTeacher($event) {
    this.studentWorkdisplayService.getAllStudentWorksForTeacher(this.teacherid, this.selectedStudent.id ).subscribe( value => {
      this.currentWorksAndMarks = value;
    });
 }


  evaluatedWork(work: WorkAndMarkModel){
   this.studentWorkdisplayService.setMark(work.workid, work.markValue).subscribe(  value => {
     console.log(value);
   }, (error) => {
     console.log(error);
   });
  }

}
