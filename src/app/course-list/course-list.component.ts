import {Component, Input, OnInit} from '@angular/core';
import {CourseModel} from '../shared/models/course.model';
import {CourseCreateService} from '../shared.services/course-create.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  private  courses: CourseModel[];

  @Input() teacherid: string;

  constructor(private courseCreateService: CourseCreateService, private router: Router  ) { }

  ngOnInit() {
    this.courseCreateService.getCourses(this.teacherid).subscribe(value => {
     this.courses = value;
     console.log(value);
    }, (error) => {
      console.log(error);
    }, () => console.log('completed'));

  }

}
