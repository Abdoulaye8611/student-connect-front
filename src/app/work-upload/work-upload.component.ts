import {Component, Input, OnInit} from '@angular/core';
import {WorkModel} from '../shared/models/work.model';
import {WorkUploadService} from '../shared.services/work-upload.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CourseModel} from '../shared/models/course.model';
import {CourseCreateService} from '../shared.services/course-create.service';
import {from} from 'rxjs';
import {map, mergeMap, toArray} from 'rxjs/operators';


@Component({
  selector: 'app-work-upload',
  templateUrl: './work-upload.component.html',
  styleUrls: ['./work-upload.component.css']
})
export class WorkUploadComponent implements OnInit {
  private courses: {hasWork: boolean; course: CourseModel; }[];
  private studentid: string;

  constructor( private courseCreateService: CourseCreateService, private workUploadService: WorkUploadService, private route: ActivatedRoute ) {
    this.studentid = this.route.snapshot.params.studentid;
  }

  ngOnInit() {
    this.courseCreateService.getAllCourses()
      .pipe(
        mergeMap( courses => {
        return from(courses)
        .pipe(mergeMap( course =>
          this.workUploadService.hasWork(this.studentid, course.id)
            .map((hasWork: boolean ) => ({hasWork: hasWork, course: course}))
        ), toArray() );
      }))
      .subscribe(value  => {
     this.courses = value ;
     console.log(value);
    }, ( error) => {
      console.log(error);
    }

    );
  }



onSelect(courseid: string, $event: any) {
  const work = new WorkModel();
  work.file = $event.target.files.item(0);
  this.workUploadService.createWork( work, this.studentid, courseid).subscribe((data) => {
  });
}

}
