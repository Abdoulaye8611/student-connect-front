import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TeacherModel} from '../shared/models/teacher.model';
import {AppSettings} from '../shared.services/AppSettings';

@Injectable()
export class TeacherdashService {
  private baseUrl = AppSettings.API_ENDPOINT;
  private headers = new HttpHeaders({'Content-Type': 'application/json'});
  private options = {headers: this.headers};
  private teacher = new TeacherModel();
  constructor(private http: HttpClient) { }


  getTeachers(): Observable <TeacherModel[]> {
    return this.http.get<TeacherModel[]>(this.baseUrl + '/teacher')
      .catch(this.errorHandler);
  }

  getTeacher(id: Number) {
    return this.http.get(this.baseUrl + '/teacher/' + id)
      .catch(this.errorHandler);
  }


  deleteTeacher(id: Number) {
    return this.http.delete(this.baseUrl + '/teacher/' + id, this.options).map((response: Response) => response)
      .catch(this.errorHandler);
  }

  createTeacher(teacher: TeacherModel) {
    return this.http.post(this.baseUrl + '/teacher', JSON.stringify(teacher), this.options).map((response: Response) => response)
      .catch(this.errorHandler);
  }

  updateTeacher(teacher: TeacherModel) {
    return this.http.put(this.baseUrl + '/teacher', JSON.stringify(teacher),  this.options)
      .catch(this.errorHandler);
  }

  errorHandler(error: Response) {

    return Observable.throw(error || 'SERVER ERROR');
  }

  setter (teacher: TeacherModel) {
    this.teacher = teacher;
  }

  getter() {
    return this.teacher;
  }

}
