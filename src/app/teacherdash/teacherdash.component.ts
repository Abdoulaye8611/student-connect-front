import { Component, OnInit } from '@angular/core';
import {TeacherModel} from '../shared/models/teacher.model';
import {Router} from '@angular/router';
import {TeacherdashService} from './teacherdash.service';

@Component({
  selector: 'app-teacherdash',
  templateUrl: './teacherdash.component.html',
  styleUrls: ['./teacherdash.component.css']
})
export class TeacherdashComponent implements OnInit {
  private teachers: TeacherModel[] = new Array();
  motCle: string = '';

  constructor(private teacherdashService: TeacherdashService, private _router: Router) { }
  ngOnInit() {
   this.teacherdashService.getTeachers().subscribe( value =>{
        this.teachers = value;
        console.log(value);
      },(error) => {
        console.log(error);
      }
    )
  }

  newTeacher(){
    const teacher = new TeacherModel();
    this.teacherdashService.setter(teacher);
    this._router.navigate(['/teacherupdate']);
  }

  deleteTeacher(teacher){
    this.teacherdashService.deleteTeacher(teacher.id).subscribe((data)=>{
      this.teachers.splice(this.teachers.indexOf(teacher), );
    }, ( error) => {
      console.log(error);
    });
  }

  updateTeacher(teacher){
    this.teacherdashService.setter(teacher);
    this._router.navigate(['teacherupdate']);
  }


  filterData(users: TeacherModel[]) {
      return users.filter(t => t.firstName === this.motCle);
    }


  doSearch(){
    this.teacherdashService.getTeachers().subscribe( value => {
        this.teachers = value;
        console.log(value);},(error) => {
        console.log(error);
      }
    )

  }


  chercher(){
    this.doSearch();
  }


}
