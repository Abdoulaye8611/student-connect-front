import { Pipe, PipeTransform } from '@angular/core';

import {TeacherModel} from '../shared/models/teacher.model';

@Pipe({ name: 'teacherFilter' })
export class TeacherPipe implements PipeTransform {


  transform(teachers: TeacherModel[] , motCle: string) {
    return teachers.filter(t => t.firstName.toLowerCase().startsWith(motCle.toLowerCase()) || t.lastName.toLowerCase().startsWith(motCle.toLowerCase()));

  }

}
