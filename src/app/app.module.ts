import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StorageServiceModule} from 'angular-webstorage-service';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { StudentNavBarComponent } from './student-nav-bar/student-nav-bar.component';
import { CreateCourseComponent } from './create-course/create-course.component';
import { StudentCourseComponent } from './student-course/student-course.component';
import { MarksComponent } from './studentmarks/studentmarks.component';
import {StudentdashService} from './studentdash/studentdash.service';
import {HttpClientModule} from '@angular/common/http';
import {LoginService} from './login/login.service';
import { StudentdashComponent } from './studentdash/studentdash.component';
import { TeacherdashComponent } from './teacherdash/teacherdash.component';
import { StudentUpdateComponent } from './student-update/student-update.component';
import { TeacherupdateComponent } from './teacherupdate/teacherupdate.component';
import {TeacherdashService} from './teacherdash/teacherdash.service';
import {UploadFileService} from './upload/upload-file.service';
import {FormUploadComponent} from './upload/form-upload.component';
import { DetailsUploadComponent } from './upload/details-upload.component';
import {TeacherPipe} from './teacherdash/TeacherPipe';
import {CourseCreateService} from './shared.services/course-create.service';
import { CourseListComponent } from './course-list/course-list.component';
import { WorkUploadComponent } from './work-upload/work-upload.component';
import {WorkUploadService} from './shared.services/work-upload.service';
import { TeacherNavBarComponent } from './teacher-nav-bar/teacher-nav-bar.component';
import { TeacherWorkdisplayComponent } from './teacher-workdisplay/teacher-workdisplay.component';
import {StudentWorkdisplayService} from './shared.services/student-workdisplay.service';
import {StudentMarksService} from './shared.services/student-marks.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    StudentNavBarComponent,
    CreateCourseComponent,
    StudentCourseComponent,
    MarksComponent,
    StudentdashComponent,
    TeacherdashComponent,
    StudentUpdateComponent,
    TeacherupdateComponent,
    FormUploadComponent,
    DetailsUploadComponent,
    TeacherPipe,
    CourseListComponent,
    WorkUploadComponent,
    TeacherNavBarComponent,
    TeacherWorkdisplayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    StorageServiceModule,
    MatButtonToggleModule,
    FlexLayoutModule
],
  providers: [StudentdashService, LoginService, TeacherdashService, UploadFileService, CourseCreateService, WorkUploadService, StudentWorkdisplayService, StudentMarksService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
