import { NgModule } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';

import {
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule
} from '@angular/material';

@NgModule({
    imports: [
        MatToolbarModule,
        MatButtonModule,
        MatInputModule,
        MatSelectModule,
        MatCardModule,
       MatSidenavModule,
      MatMenuModule,
      MatIconModule

    ],
    exports: [
        MatToolbarModule,
        MatButtonModule,
        MatInputModule,
        MatSelectModule,
        MatCardModule,
        MatSidenavModule,
        MatMenuModule,
        MatIconModule

    ]
})
export class MaterialModule { }
