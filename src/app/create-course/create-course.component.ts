import {Component, OnInit} from '@angular/core';
import {CourseCreateService} from '../shared.services/course-create.service';
import {CourseModel} from '../shared/models/course.model';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-teacher',
  templateUrl: './create-course.component.html',
  styleUrls: ['./create-course.component.css']
})
export class CreateCourseComponent implements OnInit {
  newCourseName: string;
  currentFile: File;
  teacherid: string;

  constructor(private courseCreateService: CourseCreateService, private route: ActivatedRoute) {
    this.teacherid = this.route.snapshot.params.teacherid;

  }

  ngOnInit() {
  }

  createCourse() {
    const course = new CourseModel();
    course.name = this.newCourseName;
    course.file = this.currentFile;
    this.courseCreateService.createCourse(this.teacherid, course).subscribe((data) => {
    });

  }

  onSelect($event: any) {
    this.currentFile = $event.target.files.item(0);
  }
}
