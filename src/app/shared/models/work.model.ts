export class WorkModel{
  id: string;
  name: string ;
  file: File;
  fileContent: string;
  fileName: string;
}
