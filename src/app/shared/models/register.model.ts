export class RegisterModel {
    name: String;
    lastname: String;
    email: String;
    phone: String;
    role:  String;
    password: String;
    photo: String;
}
