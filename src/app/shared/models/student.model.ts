export class StudentModel {
    id: number;
    firstName: String;
    lastName: String;
    email: String;
    phone: String;
    birthDate: Date;
    password: String;
    photo: String;
}
