export class LoginModel {
    id: number;
    email: String;
    password: String;
    type: String;


  constructor(id: number, email: String, password: String, type: String) {
    this.id = id;
    this.email = email;
    this.password = password;
    this.type = type;
  }

}

