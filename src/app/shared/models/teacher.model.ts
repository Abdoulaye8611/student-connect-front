export class TeacherModel{
    id: number;
    firstName: String;
    lastName: String;
    email: String;
    phone: String;
    birthDate: Date;
    password: String;
    photo: String;
}
