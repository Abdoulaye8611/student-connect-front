import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router}
 from '@angular/router';
import {filter} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {LoginService} from '../login/login.service';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './student-nav-bar.component.html',
  styleUrls: ['./student-nav-bar.component.css']
})
export class StudentNavBarComponent implements OnInit {
studentid: string;
userType: string;
isNavVisible: Boolean = false;
subscription: Subscription;

  constructor(private loginService: LoginService, private router: Router) {
    this.subscription = this.loginService.getLoginObservable().subscribe(user =>{
      this.studentid = user.loginId;
      this.userType = user.type;
    });

  }

  ngOnInit() {
    this.studentid = localStorage.getItem('currentUser');
    this.userType = localStorage.getItem('userType');
    this.router.events.pipe(
      filter(e => e instanceof NavigationEnd)
    ).subscribe( value => {
      console.log(location.pathname);
      this.isNavVisible = location.pathname.indexOf('auth') < 0;
    });
  }
ngOnDestroy(){
    this.subscription.unsubscribe();
}

  logout(){
    this.loginService.logout();
    this.userType = null;
    this.router.navigate(['auth/login']);
  }
}


